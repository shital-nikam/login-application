import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Switch, Link } from 'react-router-dom';
import Dashboard from './components/dashboard';
import Login from './components/login';
import Register from './components/register';
import Registerform from './components/registerform';

function App() {
  return (
    <div className="App">
      <h2 className="text-center">Welcome</h2>
     {/* Added routing below */}
      <BrowserRouter>
        <Route  exact path ="/" component={Login} />  
        <Route   path ="/login" component={Login} />  
        <Route path ="/dashboard" component={Dashboard} />
        {/* <Route path ="/userdata" component={Register} /> */}
        <Route path ="/register" component={Registerform} />
      </BrowserRouter>
    </div>
  );
}

export default App;
