import React from 'react'
import { useHistory } from 'react-router-dom'
/**
* @author
* @function dashboard
**/

const Dashboard = (props) => {
  
        const history = useHistory();
       
    

  return(
    <div>
        <h2>Dashboard</h2>
        <button type="button" className="btn btn-primary  btn-sm float-right mr-5" onClick={ () => { history.push(`/login`);}}>Log Out</button>
    </div>
   )

 }


export default Dashboard