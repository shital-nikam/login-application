import React, { Component } from 'react'
import Register from './register'


/**
* @author
* @class registerform
**/

class registerform extends Component {
    constructor(props){
        super(props);
  
        this.state = {
            username: '',
            email: '',
            occupation: ''
        };
      }
 handleParentData = (formModel) => {
   // console.log(formModel);

    this.setState({...formModel});
  }

 render() {
  return(
   <div className="card card-body col-6">
        <Register handleData={this.handleParentData}/>
          <p>{this.state.username}</p>  
          <p>{this.state.email}</p>  
          <p>{this.state.occupation}</p> 
   </div>
    )
   }
 }



export default registerform