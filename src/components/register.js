import React, { Component } from 'react'
import { Redirect , Link} from 'react-router-dom';




/**
* @author
* @class register
**/

class register extends Component {

constructor(props) {
    super(props);
    this.state = {
         username: '' ,
         email : '',
         occupation: '',
         issubmitting: false
    };

    this.handleInput = this.handleInput.bind(this);
    this.handleEmailInput = this.handleEmailInput.bind(this);
    this.handleOcpInput = this.handleOcpInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

    handleInput(event) {
        this.setState({
        username: event.target.value
        });
    }
    
    handleEmailInput(event) {
        this.setState({
        email: event.target.value
        });
    }
    handleOcpInput(event) {
        this.setState({
        occupation: event.target.value
        });
    }
    handleSubmit =  (e) => {
        e.preventDefault(); 
        this.setState({
            issubmitting :true
         })
         this.props.handleData(this.state)
         console.log(this.props)
    }

 render() {
    
 if (this.state.issubmitting) {
        return <Redirect to="/register" />
 }

  return(
    <div class="card card-body col-12">
        <form> 
            <div className="form-group">
                <label>Name</label>
                <input type="text" value={this.state.username} 
                className="form-control" onChange={this.handleInput} />
            </div>
            <div className="form-group">
                <label>Email</label>
                <input type="email" className="form-control" 
                 value={this.state.email}   onChange={this.handleEmailInput}/>
            </div> 
            <div className="form-group">
                <label>Occupation</label>
                <select className="form-control"  defaultValue={this.state.selectValue}
                onChange={this.handleOcpInput}>
                <option value="">Select Occupation</option>
                <option value="Engi">Engineer</option>
                <option value="CS">CS</option>
                <option value="CA">CA</option>
                </select>
            </div>
           
           <input type="button" value="Submit" className="btn btn-primary btn-sm" onClick={this.handleSubmit}/>
            </form>

            
    </div>
    )
   }
 }



export default register