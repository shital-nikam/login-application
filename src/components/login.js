import React, { Component } from 'react';
import { Redirect , Link} from 'react-router-dom';


class Login extends Component {
   
    constructor(props) {
        super(props);
        // create a ref to store the DOM element
        this.username = React.createRef();
        this.password = React.createRef();
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state ={
            issubmitting :false
        }
      }
    
      
        handleSubmit(e) {
            e.preventDefault();
            // alert(this.username.current.value);
            // alert(this.password.current.value);
            if(this.username.current.value === "aa" && this.password.current.value === "bb"){
               // alert("proceed for login")
              this.setState({
                  issubmitting :true
              })
            }
            else{
                this.setState({
                    issubmitting :false
                })
                alert("enter valid data")
            }
        }

     render(){

        if (this.state.issubmitting) {
            return <Redirect to="/dashboard" />
        }
        return(
        <form onSubmit={this.handleSubmit} className="card card-body col-md-6">
            <label>
            <p>Username</p>
            <input type="text"  ref={this.username}/>
            </label>
            <label>
            <p>Password</p>
            <input type="password"  ref={this.password}/>
            </label>
            <div>
                <br/>
                 <button type="submit" className="btn btn-primary btn-sm">Submit</button>
            </div>
            <div>
                <Link to="/register" className="float-right card-link">Register Here</Link>
            </div>
        </form>
        )
    }
}

export default Login